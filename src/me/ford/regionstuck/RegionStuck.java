package me.ford.regionstuck;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class RegionStuck extends JavaPlugin {
	private WorldGuardPlugin WG;
	private WorldEditPlugin WE;
	
	public void onEnable() {
		WG = (WorldGuardPlugin) getServer().getPluginManager().getPlugin("WorldGuard");
		WE = (WorldEditPlugin) getServer().getPluginManager().getPlugin("WorldEdit");
		if (WG == null) {
			Bukkit.getLogger().severe("Could not find WorldGuard - closing RegionStuck");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		if (WE == null) {
			Bukkit.getLogger().severe("Could not find WorldEdit - closing RegionStuck");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		loadConfiguration();
		Bukkit.getLogger().info("All is gooD");
	}
	
	private void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		// stuff to load (if any)
	}
	
	public boolean commandUnstuck(CommandSender sender, String[] args) {
		if (!(sender instanceof Player)) { 
			sender.sendMessage(ChatColor.RED + "Player needs to use this!");
			return true; 
		}
		Player player = (Player) sender;
		
		RegionManager rgmgr = WG.getRegionContainer().get(player.getWorld());
		//Region regions = rgmgr.getApplicableRegions(player.getLocation());s
		ApplicableRegionSet argset = rgmgr.getApplicableRegions(player.getLocation());
		LocalPlayer lplayer = //new BukkitPlayer(WE, WE.getServerInterface(), player);
		WG.wrapPlayer(player);
		ProtectedRegion frg = null;
		if (!argset.isMemberOfAll(lplayer)) {
			for (ProtectedRegion rg : argset.getRegions()) {
				if (!rg.isMember(lplayer)) {
					frg = rg;
					break;
				}
			}
			if (frg != null) {
				Location newLoc = getClosestLoc(frg, player);
				player.teleport(newLoc);
				sender.sendMessage(ChatColor.GREEN + "You are free!");
			} else {
				sender.sendMessage(ChatColor.GREEN + "You don't appear to be stuck");
			}
		} else {
			sender.sendMessage(ChatColor.GREEN + "You don't appear to be stuck");
		}
		return true;
	}
	
	private Location getClosestLoc(ProtectedRegion rg, Player player) {
		Location newLoc = player.getLocation().clone();
		BlockVector min = rg.getMinimumPoint();
		BlockVector max = rg.getMaximumPoint();
		int diffx1 = Math.abs(min.getBlockX() - player.getLocation().getBlockX());
		int diffz1 = Math.abs(min.getBlockZ() - player.getLocation().getBlockZ());
		int diffx2 = Math.abs(max.getBlockX() - player.getLocation().getBlockX());
		int diffz2 = Math.abs(max.getBlockZ() - player.getLocation().getBlockZ());
		if (Math.min(diffx1, diffx2) < Math.min(diffz1, diffz2)) {
			if (diffx1 < diffx2) {
				newLoc.setX(min.getBlockX() - 0.5);
			} else {
				newLoc.setX(max.getBlockX() + 1.5);
			}
		} else {
			if (diffz1 < diffz2) {
				newLoc.setZ(min.getBlockZ() - 0.6);
			} else {
				newLoc.setZ(max.getBlockZ() + 1.5);
			}
		}
		try {
			newLoc.setY(getSuitbaleY(newLoc));
		} catch (Exception e) {
			Bukkit.getLogger().severe("Non-AIR block not found for unstuck players");
		}
		return newLoc;
	}
	
	private int getSuitbaleY(Location loc) throws Exception {
		Location cloc = loc.clone();
		for (int y = 255; y > 0; y--) {
			cloc.setY(y);
			if (cloc.getBlock().getType().isSolid()) {
				return y + 1;
			}
		}
		throw new Exception("BlockNotFound");
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("unstuck")) {
			return commandUnstuck(sender, args);
		}
		
		return false;
	}

}
