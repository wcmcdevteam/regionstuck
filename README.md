# RegionStuck v0.0.1

Get unstuck from a WorldGuard region you're not a member of.

`/stuck` or `/unstuck` to get unstuck

### Commands and permissions
```
Command             	Permission              	Description                             
unstuck             	stuck.unstuck           	Get unstuck in a region you're not a member of
```

### Permissions
```
Permission              	Children                                        	Descrpition                             
stuck.unstuck           	None                                            	Allow using the unstuck command         
```